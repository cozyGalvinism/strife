class Enemy:

    def __init__(self, name, health, strength):
        self.name = name
        self.health = health
        self.strength = strength

        self.current_hp = self.health
    
    def aggrieve(self, damage):
        if self.current_hp - damage < 0:
            self.current_hp = 0
        else:
            self.current_hp = self.current_hp - damage
        return True
    
    def __str__(self):
        return f'<Enemy: {self.name}>'