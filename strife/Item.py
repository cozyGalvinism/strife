class Item:

    def __init__(self, key, name, description, worth):
        self.key = key
        self.name = name
        self.description = description
        self.worth = worth
    
    def __str__(self):
        return f'<Item: {self.name}>'

class Weapon(Item):

    def __init__(self, key, name, description, worth, damage):
        super(Weapon, self).__init__(key, name, description, worth)
        self.damage = damage