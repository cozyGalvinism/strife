CREATE TABLE IF NOT EXISTS `users` (
	`discord_id` INTEGER,
	`experience` INTEGER NOT NULL,
	`gel_viscosity` INTEGER NOT NULL,
	`mangrit` INTEGER NOT NULL,
	`boondollars` INTEGER NOT NULL,
	`magic` INTEGER NOT NULL,
	`selected_weapon` TEXT NOT NULL,
	PRIMARY KEY(`discord_id`)
) WITHOUT ROWID;

CREATE TABLE IF NOT EXISTS `grist_torrent` (
	`user_id` INTEGER,
	`grist_type` TEXT,
	`amount` INTEGER NOT NULL,
	PRIMARY KEY(`user_id`, `grist_type`),
	FOREIGN KEY(`user_id`) REFERENCES `users`(`discord_id`)
) WITHOUT ROWID;

CREATE TABLE IF NOT EXISTS `inventory` (
	`user_id` INTEGER,
	`item` TEXT,
	`amount` INTEGER NOT NULL,
	PRIMARY KEY(`user_id`, `item`),
	FOREIGN KEY(`user_id`) REFERENCES `users`(`discord_id`)
) WITHOUT ROWID;