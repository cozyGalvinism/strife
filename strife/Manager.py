import enum

class StrifeState(enum.Enum):
    LFG = 0
    STRIFING = 1
    NONE = 2

class StrifeManager:

    def __init__(self):
        self.state = StrifeState.NONE
        self.users = []
        self.enemy = None
    
    @property
    def running(self):
        return self.state != StrifeState.NONE

    def start_strife(self):
        if self.running: 
            return False
        
        self.running = True
        return True
    
    def participate(self, user_id):
        if not self.running:
            return False
        self.users.append(user_id)
        return True
    
    def aggress(self, user_id):
        if not self.running:
            return False
    
    def abjure(self, user_id):
        if not self.running:
            return False
    
    def abscond(self, user_id):
        if not self.running:
            return False