import math
from . import Item

class User:

    def __init__(self, id, experience = 0, gel_viscosity = 20, mangrit = 1, items = {}, equipped_weapon=None, boondollars = 0, grist_cache = {}, magic = 0):
        self.id = id
        self.experience = experience
        self.gel_viscosity = gel_viscosity
        self.mangrit = mangrit
        self.items = items
        self.boondollars = boondollars
        self.grist_cache = grist_cache
        self.equipped_weapon = equipped_weapon
        self.magic = magic

        self.current_hp = self.gel_viscosity
    
    @property
    def level(self):
        return 1.15 * math.sqrt(self.experience)
    
    @property
    def attack_damage(self):
        if self.equipped_weapon is None or not isinstance(self.equipped_weapon, Item.Weapon):
            return self.mangrit
        else:
            return self.mangrit + self.equipped_weapon.damage
    
    @property
    def ability_power(self):
        pass
    
    @property
    def luck(self):
        pass
    
    def aggress(self, damage):
        if self.current_hp - damage < 0:
            self.current_hp = 0
        else:
            self.current_hp = self.current_hp - damage
        return True