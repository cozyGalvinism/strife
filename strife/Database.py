import os
import json
import sqlite3
from os.path import join
from .Item import Item, Weapon


class StrifeDB:

    def __init__(self):
        self.items = {}
        self.grist_types = {}
        self.strife_dir = os.path.dirname(__file__)

        if os.path.isfile(os.path.join(self.strife_dir, 'items.json')):
            with open(os.path.join(self.strife_dir, 'items.json'), 'r', encoding='utf-8-sig') as f:
                f_items = json.load(f)
                for k, f_itm in f_items.items():
                    item = None
                    if f_itm['type'].lower() == 'weapon':
                        item = Weapon(k, f_itm['name'], f_itm['description'], f_itm['worth'], f_itm['damage'])
                    else:
                        item = Item(k, f_itm['name'], f_itm['description'], f_itm['worth'])
                    self.items.update(k, item)
        
        if os.path.isfile(os.path.join(self.strife_dir, 'grists.json')):
            with open(os.path.join(self.strife_dir, 'grists.json'), 'r', encoding='utf-8-sig') as f:
                f_grist = json.load(f)
                for k, grist in f_grist.items():
                    self.grist_types.update(k, grist)

        self.conn = sqlite3.connect('STRIFE.db')
        self.curs = self.conn.cursor()
    
    def get_user(self, user_id):
        db_user = self.curs.execute("SELECT * FROM `users` WHERE `discord_id` = ?;", (user_id,)).fetchone()
        db_grist = self.curs.execute("SELECT * FROM `grist_torrent` WHERE `user_id` = ?;", (user_id,)).fetchall()
        db_inv = self.curs.execute("SELECT * FROM `inventory` WHERE `user_id` = ?;", (user_id,)).fetchall()

    def close(self):
        self.curs.close()
        self.conn.close()