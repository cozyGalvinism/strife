# STRIFE!

A Homestuck-inspired RPG bot!

This is an entry for the Discord Hack Week 2019!

## Installing

1. Install `pipenv`
2. Clone this repo
3. Run `pipenv install`
4. Run the bot with `pipenv run strife`
5. ???
6. Profit!

---

Made with :heart: and Python by cozyGalvinism