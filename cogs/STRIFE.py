import os
import discord
from discord.ext import commands

class STRIFE(commands.Cog):

    def __init__(self, bot, strife_db):
        self.bot = bot
        self.strife_db = strife_db
    
    @commands.command(pass_context=True, aliases=['hometrash', 'homosuck'], case_insensitive=True)
    async def homestuck(self, ctx):
        await ctx.send('Homestuck? In my 2019?! More likely than you think: https://www.homestuck.com/story')
    
    @commands.command(pass_context=True, case_insensitive=True)
    async def strife(self, ctx):
        pass

def setup(bot):
    bot.add_cog(STRIFE(bot))